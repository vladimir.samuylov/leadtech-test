<?php
/**
 * Created by PhpStorm.
 * User: Yury.Golyandin
 * Date: 30.09.2020
 * Time: 20:47
 */
require __DIR__ . '/vendor/autoload.php';

//По заданию обработка "в лоб" невозможна.
//Подключаем библиотеку для реализации асинхронности основанной на PCNTL расширении. https://github.com/spatie/async
//Можно взять варианты с https://amphp.org/ или https://reactphp.org/ но для текущей задачи больше всего подходит вариант выбранный мною.

if (!\Spatie\Async\Pool::isSupported()) {
    throw new \Exception('async execution not supported');
}
$app = new \App\App();
$app->run();