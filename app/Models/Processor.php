<?php
/**
 * Created by PhpStorm.
 * User: Yury.Golyandin
 * Date: 30.09.2020
 * Time: 21:01
 */

namespace App\Models;


use LeadGenerator\Lead;

class Processor
{
    /**
     * @var
     */
    private static $instance;


    private function __construct()
    {

    }

    public static function getInstance(): Processor
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Функция обработки заявки
     * @param Lead $lead Заявка
     * @param array $disabledCategory Список запрещенных к обработке категорий
     * @return Result Результат
     * @throws \Exception
     */
    public function process(Lead $lead, $disabledCategory=[]){
        if (in_array($lead->categoryName, $disabledCategory)) {
            throw new \Exception('Category,' . $lead->categoryName . ' is disabled, failed task id ' . $lead->id);
        }

        // обработчик засыпает
        sleep(2);

        return new Result($lead);
    }
}