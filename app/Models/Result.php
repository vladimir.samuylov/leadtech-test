<?php
/**
 * Created by PhpStorm.
 * User: Yury.Golyandin
 * Date: 30.09.2020
 * Time: 21:26
 */

namespace App\Models;
use LeadGenerator\Lead;
use Carbon\Carbon;

class Result implements ResultInterface
{
    /**
     * @var Lead
     */
    private $lead;

    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
    }

    /**
     * Реализация метода вывода результата обработки задачи.
     * @return string
     */
    public function toString(): string
    {
       return implode(' | ',[
           $this->lead->id,
           $this->lead->categoryName,
           Carbon::now()
       ]);
    }
}