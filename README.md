# Тестовое задание для LeadTech
## Требования
✅ Объектно-ориентированный подход, интерфейсы

✅ Нельзя использовать PHP-фреймворки

✅ Допускается использование подключаемых библиотек.

✅ Docker для запуска проекта

✅ Type Hinting, PSR

✅ Залить код в публичный Git-репозиторий


## Подготовка
```$xslt
git clone git@gitlab.com:zubat/leadtech-test.git
cd leadtech-test
composer install
```
## Запуск
Запуск для cmd
```
docker run -v %cd%/logs:/home/src/logs lead
```

Запуск для sh
```
docker run -v $(pwd)/logs:/home/src/logs lead
```
