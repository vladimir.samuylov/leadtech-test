FROM php:7.3-cli

RUN docker-php-ext-configure pcntl --enable-pcntl \
    && docker-php-ext-install \
     pcntl

RUN mkdir -p /home/src

WORKDIR /home/src

COPY . /home/src

CMD ["php", "./run.php"]